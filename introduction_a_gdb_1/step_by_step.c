#include<stdlib.h>
#include<stdio.h>

double ComputeSquareDelta(double a, double b, double c) {
	double squareDelta = 0.0;

	squareDelta = b * b - 4 * a * c;
	return squareDelta;
}

void ReadArguments(int argc, char* argv[], double * val) {
	int test = 0;
	printf("argc = %d\n", argc);
	if (argc != 2) {
		printf("please enter 1 argument please! \n");
		exit(-1);
	}

	printf("%s\n%s\n", argv[0], argv[1]);
	test = sscanf(argv[1], "%lf", val);
	if (test != 1) {
		printf("sorry we could not read your argument! \n");
		exit(-1);
	}
}

struct data_t {
	double x;
	int id;
};
typedef struct data_t data_t;

int main(int argc, char* argv[]) {
	int i = 0;
	int j = 0;
	double * ptr = NULL;
	double A[4] = { 0.1, 3.45, 2.1, -10.3 };
	data_t data = { .x = 3.25, .id = 3 };
	double aa = 0.0;
	double bb = 0.0;
	double cc = 0.0;
	double sqDelta = 0.0;
	int test = 0;

	ReadArguments(argc, argv, &aa);

	aa = 1.0;

	bb = 2.1;
	cc = -2.3;
	ptr = A;

	i++;
	j--;

	sqDelta = ComputeSquareDelta(aa, bb, cc);

	A[3] = A[2] * 0.2 * data.x;
	A[1] = A[1] * 0.2 * data.id;
	A[0] = *(ptr + 1) + 1.3;

	printf("result = %g \n", sqDelta);

	return 0;
}
