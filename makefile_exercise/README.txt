1) Ecrire un fichier en-tête

1-a) Ce fichier doit définir le type suivant

/* ------------------------------ */
typedef struct foo_t foo_t;

struct foo_t {
	int val_i;
	double val_d;
	char name[10];
};
/* ------------------------------ */

1-b) Ce fichier doit déclarer les fonctions des fichiers

io.c
routines.c

2) Ecrire un fichier makefile

pour compiler
	io.c
	routines.c
	main.c

pour créer une bibliothèque libfoo.a qui regroupe les objets de
	io.c
	routines.c

pour créer exécutable foo.exe à partir des objets de
	libfoo.a
	main.o