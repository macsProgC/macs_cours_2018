#include <stdio.h>
#include "myheader.h"


int routines_A(int x) {
	printf("this is the routines A the argument is x=%d\n", x);
	return x + 1;
}

void routines_B(double *ptr, int i) {
	printf("this is the routines A the argument is ptr=%p i=%d\n", ptr, i);
}
