#include<stdlib.h>
#include<stdio.h>

#define REPLACE_ME_INT 0
#define REPLACE_ME_DOUBLE 0

/* definition des types */
typedef struct state_t state_t ;
struct state_t {
    double x;
    double y;
    double z;
    double t;
    };

/* declaration des fonctions */
void Initialize(state_t *pS,double x0,double y0,double z0,double t0);
int IsNotOver(state_t * pS) ;
void UpdateState(state_t * pS);
void PrintStateIfNeeded(state_t * pS);
double Get_x (state_t* pS) ;
double Get_y (state_t* pS) ;
double Get_z (state_t* pS) ;
double Get_t (state_t* pS) ;
void Set_x(state_t* pS, double val_x);
void Set_y(state_t* pS, double val_y);
void Set_z(state_t* pS, double val_z);
void Set_t(state_t* pS, double val_t);


/* definition des fonctions */

void Set_x(state_t* pS, double val_x) {
    pS->x = val_x;
    }

void Set_y(state_t* pS, double val_y) {
    pS->y = val_y;
    }

void Set_z(state_t* pS, double val_z) {
    pS->z = val_z;
    }

void Set_t(state_t* pS, double val_t) {
    pS->t = val_t;
    }



double Get_t (state_t* pS) {
    return pS->t ;
    }

double Get_x (state_t* pS) {
    return pS->x ;
    }

double Get_y (state_t* pS) {
    return pS->y ;
    }

double Get_z (state_t* pS) {
    return pS->z ;
    }

void Initialize(state_t *pS,double x0,double y0,double z0,double t0) {
    Set_x(pS, x0);
    Set_y(pS, y0);
    Set_z(pS, z0);
    Set_t(pS, t0);
    }

int IsNotOver(state_t * pS) {
    const double tMax = 100.0;
    double t = Get_t(pS);
    return (t <= tMax);
    }

void UpdateState(state_t * pS) {
    double x = Get_x(pS);
    double y = Get_y(pS);
    double z = Get_z(pS);
    double t = Get_t(pS);
    double new_x = 0.0;
    double new_y = 0.0;
    double new_z = 0.0;
    double new_t = 0.0;
    const double a = 10.0;
    const double b = 28.0;
    const double c = 8.0/3.0;
    const double dt = 1e-2;

    new_x = x + a*(y-x)*dt;
    new_y = y + (x*(b-z) - y)*dt;
    new_z = z + (x*y - c*z)*dt;
    new_t = t + dt;

    Set_x(pS, new_x);
    Set_y(pS, new_y);
    Set_z(pS, new_z);
    Set_t(pS, new_t);

    }

void PrintStateIfNeeded(state_t * pS) {
    double x = Get_x(pS);
    double y = Get_y(pS);
    double z = Get_z(pS);
    double t = Get_t(pS);

    printf("%g %g %g %g\n",t,x,y,z);
    }


int main(void) {
    state_t S;
    double x0 = 0.1;
    double y0 = 0.0;
    double z0 = 0.0;
    double t0 = 0.0;

    Initialize(&S,x0,y0,z0,t0);

    while(IsNotOver(&S)) {
        UpdateState(&S);
        PrintStateIfNeeded(&S);
        }
    return EXIT_SUCCESS;
    }








