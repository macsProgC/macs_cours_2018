#include <stdlib.h>
#include <stdio.h>
#include "algebra.h"
#include "math.h"

//TODO
Vect_t* CreateVect(int size) {
    Vect_t* v = NULL;
    v = malloc(sizeof(Vect_t));
    v->size = size;
    v->data = malloc(sizeof(double)*(v->size));

    return v;
    }

double GetVectNorm(Vect_t* pv) {
    double res = 0;
    int size = GetVectSize(pv);
    int i = 0;
    for(i = 0; i<size; i++) {
        double coordVal = GetVectCoord(pv,i);
        res = res + coordVal * coordVal ;
        }
    return sqrt(res);
    }

void ResizeVect(Vect_t* pv, int newSize) {
//TODO
    }


void PrintfVect(Vect_t* pv) {
    int size = GetVectSize(pv);
    int i = 0;
    printf("size = %d\n",size);
    for(i = 0; i<size; i++) {
        double coordVal = GetVectCoord(pv,i);
        printf("coord[%d] = %lf\n",i, coordVal);
        }
    }

void SetVectCoord(Vect_t* pv,int coord, double val) {
    (pv->data)[coord] = val;
    }

double GetVectCoord(Vect_t* pv, int coord) {
    return (pv->data)[coord];
    }

void DestroyVect(Vect_t* pv) {
    free(pv->data);
    pv->data = NULL;
    pv->size = 0;
    }


int GetVectSize(Vect_t* pv) {
    return pv->size;
    }


