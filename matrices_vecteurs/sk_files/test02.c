#include<stdlib.h>
#include<stdio.h>
#include "algebra.h"


int main(void) {
    Vect_t* v = CreateVect(10);
    int size = 0;

    size = GetVectSize(v);
    printf("size = %d\n",size);

    DestroyVect(v);
    free(v);
    v = NULL;

    return 0;
    }
;
