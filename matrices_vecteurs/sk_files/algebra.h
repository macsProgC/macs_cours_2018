#ifndef ALGEBRA_H
#define ALGEBRA_H


struct Vect_t {
    int size;
    double* data;
    };
typedef struct Vect_t Vect_t;

struct Matrix_t {
    int useless;
    };
typedef struct Matrix_t Matrix_t ;


Vect_t* CreateVect(int size);
/* TODO: Detruire vecteur */
int GetVectSize(Vect_t* pv);
double GetVectCoord(Vect_t* pv, int coord);
void SetVectCoord(Vect_t* pv,int coord, double val);
void PrintfVect(Vect_t* pv);
void ResizeVect(Vect_t* pv, int newSize);
void DestroyVect(Vect_t* pv);
double GetVectNorm(Vect_t* pv);

Matrix_t* CreateMatrix(int nbLines, int nbCol) ;
double GetMatrixElt(Matrix_t* m, int i, int j);
void SetMatrixElt(Matrix_t* m, int i, int j, double val);

#endif
//ALGEBRA_H
