#include<stdlib.h>
#include "algebra.h"
#include<stdio.h>

int main(void) {
    int size = 10;
    Vect_t* v = CreateVect(size);
    double res = 0.0;
    int i = 5;
    double val = 13.867;
    int newSize = 15;

    SetVectCoord(v,i,val);

    res = GetVectCoord(v, i);
    printf("coord %d = %g\n", i, res);


    PrintfVect(v);

    //ResizeVect(v, newSize); //TODO

    for(i=0; i<GetVectSize(v); i++) {
        SetVectCoord(v,i,1.0);
        }

    res = GetVectNorm(v);
    printf("norm = %g\n",res);


    DestroyVect(v);

    free(v);
    v = NULL;

    return 0;
    }
;
